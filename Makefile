all:
	make clean
	cp -f *.c ~/opt/rsf/madagascar-src/user/cdacosta/
	cd ~/opt/rsf/madagascar-src/; \
	  scons install

clean:
	rm -rf ~/opt/rsf/madagascar-src/build/user/cdacosta/*
