irtt2z
====

This program aims to implement the time-to-depth conversion and depth velocity
estimation based on [Iversen and Tygel (2008)](http://library.seg.org/doi/abs/10.1190/1.2907736).
