/* irtt2z */

/*
  Copyright (C) 2013 Carlos Alberto da Costa Filho

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <math.h>
#include <cblas.h>
#include <lapacke.h>
#include <rsf.h>
#include "irtt2z.h"

typedef struct {
    float *xpos, *ypos, *zpos;
    float *v;
} ray;

int main (int argc, char *argv[])
{
    bool verb;
    float azim;

    sf_axis at, ax, ay, az;
    sf_axis ag1, ag2;
    int nt, nx, ny, nz;
    float ot;
    int ng1, ng2;
    float dt, dx, dy, dz;
    float dg1, dg2;
    float og1, og2;
    int it, ix, iy, iz;
    int ig1, ig2;

    int ii,jj;

    /* Ray-tracing parameters */
    float h = 0.1; /* Step size */
    float u[2];
    int ir;
    ray *rays;
    ray *r;
    float Q1[2][2];
    float Q2[2][2];
    float P1[2][2];
    float P2[2][2];
    float A, B, F;
    float x[3]; /* Cartesian coordinates (x, y, z) */
    float p[3]; /* Slowness vector (px, py, pz) */
    float e1[3];
    float e2[3];
    float e3[3];
    /* Auxiliary */
    float dAdt, dBdt, dFdt;
    float dvdg1, dvdg2, dvdt, dvdg1g1, dvdg1g2, dvdg2g2;
    float normp;
    float paux[3], e1aux[3];
    float dvdX[3];
    float Qq[3][3], H[3][3], QxT[3][3];
    float M1[2][2], Jv[2][2], V[2][2], Q1i[2][2], VQ1[2][2], VQ2[2][2];
    float detQ1;
    int ipiv[3];


    float **vmig=NULL; /* input time migration velocity */
    float **vdep=NULL; /* output depth velocity  */
    float **vdix=NULL; /* Dix velocity conversion of vmig */

    float **vdix_dt=NULL; /* Dix velocity derivatives */
    float **vdix_dg1=NULL;
    float **vdix_dg2=NULL;
    float **vdix_dg1g1=NULL;
    float **vdix_dg1g2=NULL;
    float **vdix_dg2g2=NULL;

    sf_file Fvmig=NULL;  /* input time migration velocity file */
    sf_file Fvdep=NULL; /* output depth velocity file */
    sf_file Fvdix=NULL; /* output depth */

    /*------------------------------------------------------------*/
    sf_init(argc,argv);
    if (! sf_getbool("verb",  &verb)) verb=false; /* Verbosity flag */
    if (! sf_getfloat("azim", &azim)) azim=0.0; /* Azimuth */
    if (NULL != sf_getstring("vdix")) {
    /* Dix velocity file */
        Fvdix = sf_output("vdix");
    } else {
        if(verb) sf_warning("No Dix file.");
    }

    /* Input */
    Fvmig = sf_input("in");

    /* t axis */
    at = sf_iaxa(Fvmig, 1);
    sf_setlabel(at, "t");
    ot = sf_o(at);
    nt = sf_n(at);
    dt = sf_d(at);
    if(verb) sf_raxa(at);

    /* x axis */
    ax = sf_iaxa(Fvmig, 2);
    sf_setlabel(ax, "x");
    nx = sf_n(ax);
    dx = sf_d(ax);
    if(verb) sf_raxa(ax);

    /* y axis */
    ay = sf_maxa(1, 0, 1);
    sf_setlabel(ay, "y");
    ny = sf_n(ay);
    if(verb) sf_raxa(ay);

    /* g1 axis */
    ag1 = sf_maxa(1, 0, 1);
    sf_copyaxis(ag1, ax);
    sf_setlabel(ag1, "g1");
    ng1 = sf_n(ag1);
    dg1 = sf_d(ag1);
    og1 = sf_o(ag1);
    if(verb) sf_raxa(ag1);

    /* g2 axis */
    ag2 = sf_maxa(1, 0, 1);
    /* Gives deferencing error. */
    /*ag2 = (sf_axis) sf_alloc(1, sizeof(*ag2));*/
    sf_copyaxis(ag2, ay);
    sf_setlabel(ag2, "g2");
    ng2 = sf_n(ag2);
    dg2 = sf_d(ag2);
    og2 = sf_o(ag2);
    if(verb) sf_raxa(ag2);

    /* Setup input time migration velocity */
    vmig = sf_floatalloc2(nt, nx);
    sf_floatread(vmig[0], nt*nx, Fvmig);
    if(verb) sf_warning("Input allocation successful.");

    irtt2z_init(vmig, at, ax, ay, ag1, ag2, azim, h);
    if(verb) sf_warning("Initialized irtt2z.c.");

    /* Setup output axes for vdix */
    if (NULL != Fvdix){
        sf_oaxa(Fvdix, at, 1); if(verb) sf_raxa(at);
        sf_oaxa(Fvdix, ax, 2); if(verb) sf_raxa(ax);
    }
    /* Calculate vdix and its derivatives */
    vdix       = irtt2z_dix(vmig);
    vdix_dt    = irtt2z_dt(vdix);
    vdix_dg1   = irtt2z_dg1(vdix);
    vdix_dg2   = irtt2z_dg2(vdix);
    vdix_dg1g1 = irtt2z_dg1g1(vdix);
    vdix_dg1g2 = irtt2z_dg1g2(vdix);
    vdix_dg2g2 = irtt2z_dg2g2(vdix);
    if (verb) sf_warning("Calculated dix velocity.");

    /* Setup output depth velocity */
    Fvdep = sf_output("out");
    vdep = sf_floatalloc2(nt, nx);;
    sf_oaxa(Fvdep, ax, 2); if(verb) sf_raxa(ax);

    /* Initial conditions */
    u[0] = cos(azim); u[1] = sin(azim);

    /* Allocate memory for fays */
    rays = sf_alloc(ng1*ng2, sizeof(ray));
    if (verb) sf_warning("Allocated rays.");
    
    /* Iterate rays */
    for (ig2=0; ig2<ng2; ig2++){
        for (ig1=0; ig1<1; ig1++){
            r = &rays[ig1+ig2*ng2];
            r->xpos = sf_floatalloc(nt);
            r->ypos = sf_floatalloc(nt);
            r->zpos = sf_floatalloc(nt);
            r->v    = sf_floatalloc(nt);
            if (verb) sf_warning("Allocated xpos, ypos, zpos and v for ray %d.",
                                 ig1+ig2*ng2);

            /* Iteration 0 */
            r->v[0] = vdix[ig1][0];

            r->xpos[0] = og1 + ig1*dg1;
            r->ypos[0] = og2 + ig2*dg2;
            r->zpos[0] = 0;

            p[0] = 0;
            p[1] = 0;
            p[2] = 1/vdix[ig1][0];

            /* Q1 = P2 = I and Q2 = P1 = 0 */
            for (ii=0; ii<2; ii++){
                for (jj=0; jj<2; jj++){
                    Q1[ii][jj] = P2[ii][jj] = Q2[ii][jj] = P1[ii][jj] = 0;
                }
                Q1[ii][ii] = P2[ii][ii] = 1;
            }

            if (verb) sf_warning("ig2=%d, ig1=%d\nx=(%f, %f, %f)"
                                 "\np=(%f, %f, %f)"
                                 "\nvdix=%f",
                                 ig2, ig1, r->xpos[0], r->ypos[0], r->zpos[0],
                                 p[0], p[1], p[2], r->v[0]);

            e1[0] = 1; e1[1] = 0; e1[2] = 0;
            e3[0] = 0; e3[1] = 0; e3[2] = 1;
            irtt2z_cross(e3, e1, e2);

                    A = 1;
                    B = 0;
                    F = 1;

            /* Iterate over time */
            for (it=1; it<2; it++){
                /* Step 3 */
                /*if (verb) sf_warning("it = %d", it);*/
                /*if (it==1){*/
                    /*if (verb) sf_warning("A = %.2f", A);*/
                    /*if (verb) sf_warning("B = %.2f", B);*/
                    /*if (verb) sf_warning("F = %.2f", F);*/
                /*} else {*/
                /*}*/
                /* Step 4 */
                /* v = F*vdix */
                r->v[it] = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt, vdix);
                if (verb) sf_warning("r->v[%d] = %f", it, r->v[it]);

                /* Step 5 */
                /* Solves dx/dT = v^2 p */
                r->xpos[it] = irtt2z_rk4_v2p(r->xpos[it-1], r->v[it], p[0]);
                r->ypos[it] = irtt2z_rk4_v2p(r->ypos[it-1], r->v[it], p[1]);
                r->zpos[it] = irtt2z_rk4_v2p(r->zpos[it-1], r->v[it], p[2]);
                if (verb) sf_warning("r->xpos[%d] = %f", it, r->xpos[it]);
                if (verb) sf_warning("r->ypos[%d] = %f", it, r->ypos[it]);
                if (verb) sf_warning("r->zpos[%d] = %f", it, r->zpos[it]);

                /* Solves dQ_I/dT = v^2 P_I */
                for (ii=0; ii<2; ii++){
                    for (jj=0; jj<2; jj++){
                        Q1[ii][jj] = irtt2z_rk4_v2p(Q1[ii][jj], r->v[it],
                                                    P1[ii][jj]);
                        Q2[ii][jj] = irtt2z_rk4_v2p(Q2[ii][jj], r->v[it],
                                                    P2[ii][jj]);
                        if (verb) sf_warning("Q1_%d%d = %f",ii,jj,Q1[ii][jj]);
                    }
                }

                for (ii=0; ii<2; ii++){
                    for (jj=0; jj<2; jj++){
                        if (verb) sf_warning("Q2_%d%d = %f",ii,jj,Q2[ii][jj]);
                    }
                }
                A = irtt2z_A(Q1, Q2, u);
                B = irtt2z_B(Q2, u);
                if (verb) sf_warning("A = %.2f", A);
                if (verb) sf_warning("B = %.2f", B);
                if (B > 0){
                    F = A/sqrt(B);
                    if (verb) sf_warning("F = %.2f", F);
                } else {
                    if (verb) sf_warning("Breaking off loop");
                    break;
                }

                /* Steps 6, 7, 8, 9 */
                dAdt = -(r->v[it])*(r->v[it])*B;
                dBdt = -2*(r->v[it])*(r->v[it])*irtt2z_dBdt(Q2, P2, u);
                dFdt = dAdt*sqrt(B) - dBdt*(A/2)/sqrt(B*B*B);
                dvdg1 = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt, vdix_dg1);
                dvdg2 = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt, vdix_dg2);
                dvdt  = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt, vdix_dt);
                dvdt += dFdt*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt, vdix);
                for (ii=0; ii<3; ii++){
                    for (jj=0; jj<3; jj++){
                        if ((ii<2) && (jj<2)){
                            Qq[ii][jj] = Q1[ii][jj];
                        } else if ((ii==2) && jj==2){
                            Qq[ii][jj] = r->v[it];
                        } else {
                            Qq[ii][jj] = 0;
                        }
                        if (verb) sf_warning("Qq_%d%d = %f",ii,jj,Qq[ii][jj]);
                    }
                    H[ii][0] = e1[ii];
                    H[ii][1] = e2[ii];
                    H[ii][2] = e3[ii];
                }
                for (ii=0; ii<3; ii++){
                    for (jj=0; jj<3; jj++){
                        if (verb) sf_warning("H_%d%d = %f",ii,jj,H[ii][jj]);
                    }
                }
                if (verb) sf_warning("dAdt = %f", dAdt);
                if (verb) sf_warning("dBdt = %f", dBdt);
                if (verb) sf_warning("dFdt = %f", dFdt);
                if (verb) sf_warning("dvdg1 = %f", dvdg1);
                if (verb) sf_warning("dvdg2 = %f", dvdg2);
                if (verb) sf_warning("dvdt = %f", dvdt);

                /* Step 10 */
                /* (Qx)^T = (H Qq)^T = Qq^T H^T*/
                cblas_sgemm(CblasRowMajor, CblasTrans, CblasTrans, 3, 3, 3, 1.0,
                            (float *) Qq, 3,
                            (float *) H,  3, 0.0,
                            (float *) QxT, 3);
                /* Calculate dvdX = (Qx^T)^(-1) dvdG */
                dvdX[0] = dvdg1;
                dvdX[1] = dvdg2;
                dvdX[2] = dvdt;
                LAPACKE_sgesv(LAPACK_ROW_MAJOR, 3, 1,
                              (float *) QxT, 3, (int *)ipiv, dvdX, 1);
                for (ii=0; ii<3; ii++){
                    if (verb) sf_warning("dvdX[%d] = %f", ii, dvdX[ii]);
                }

                /* Step 11 */
                /* Solves dp/dT = -(1/v)*dv/dx */
                normp = 0;
                for (ii=0; ii<3; ii++){
                    paux[ii] = p[ii];
                    p[ii] = p[ii] - h*dvdX[ii]/r->v[it];
                    e1aux[ii] = e1[ii];
                    normp += p[ii]*p[ii];
                    if (verb) sf_warning("p[%d] = %f", ii, p[ii]);
                }
                normp = sqrt(normp);
                /* Solves de1/dT = -v^2 (e1.dpdt) p, e3 = p/|p| */
                for (ii=0; ii<3; ii++){
                    e1[ii] = e1aux[ii] + irtt2z_rk4_de1dt(e1aux, r->v[it], paux,
                                                          p, ii);
                    e3[ii] = p[ii]/normp;
                    if (verb) sf_warning("e3[%d] = %f", ii, e3[ii]);
                }
                for (ii=0; ii<3; ii++){
                    if (verb) sf_warning("e1[%d] = %f", ii, e1[ii]);
                }
                /* Must to regularize e1 by subtracting its projection on e3 */
                irtt2z_cross(e3, e1, e2);
                for (ii=0; ii<3; ii++){
                    if (verb) sf_warning("e2[%d] = %f", ii, e2[ii]);
                }

                /* Step 12  */
                dvdg1g1 = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt,
                                            vdix_dg1g1);
                dvdg1g2 = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt,
                                            vdix_dg1g2);
                dvdg2g2 = F*irtt2z_l_interp(r->xpos[it], ot+(it-1)*dt,
                                            vdix_dg2g2);
                Jv[0][0] = dvdg1g1;
                Jv[0][1] = Jv[1][0] = dvdg1g2;
                Jv[1][1] = dvdg2g2;
                detQ1 = Q1[0][0]*Q1[1][1] - Q1[0][1]*Q1[1][0];
                Q1i[0][0] = Q1[1][1]/detQ1;
                Q1i[0][1] = -Q1[1][0]/detQ1;
                Q1i[1][0] = -Q1[0][1]/detQ1;
                Q1i[1][1] = Q1[0][0]/detQ1;
                /* M1 = P1 Q1^(-1) */
                cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                            2, 2, 2, 1.0,
                            (float *) P1, 2,
                            (float *) Q1i,  2, 0.0,
                            (float *) M1, 2);
                /* V = Jv Q1^(-1) */
                cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                            2, 2, 2, 1.0,
                            (float *) Jv, 2,
                            (float *) Q1i,  2, 0.0,
                            (float *) V, 2);
                /* V = Q1^T Jv Q1^(-1) */
                cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans,
                            2, 2, 2, 1.0,
                            (float *) Q1, 2,
                            (float *) V,  2, 0.0,
                            (float *) V, 2);
                /* V = Q1^T Jv Q1^(-1) + M1*dvdt*/
                for (ii=0; ii<2; ii++){
                    for (jj=0; jj<2; jj++){
                        V[ii][jj] += M1[ii][jj]*dvdt;
                    }
                }

                /* Step 13 */
                cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans,
                            2, 2, 2, 1.0,
                            (float *) V, 2,
                            (float *) Q1,  2, 0.0,
                            (float *) VQ1, 2);
                cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans,
                            2, 2, 2, 1.0,
                            (float *) V, 2,
                            (float *) Q2,  2, 0.0,
                            (float *) VQ2, 2);
                /* Solves dP_I/dT = -Q_I/v */
                for (ii=0; ii<2; ii++){
                    for (jj=0; jj<2; jj++){
                        P1[ii][jj] = irtt2z_rk4_dPdt(P1[ii][jj], r->v[it],
                                                     VQ1[ii][jj]);
                        P2[ii][jj] = irtt2z_rk4_dPdt(P2[ii][jj], r->v[it],
                                                     VQ2[ii][jj]);
                        if (verb) sf_warning("P1_%d%d = %f",ii, jj, P1[ii][jj]);
                    }
                }
                for (ii=0; ii<2; ii++){
                    for (jj=0; jj<2; jj++){
                        if (verb) sf_warning("P2_%d%d = %f",ii, jj, P2[ii][jj]);
                    }
                }
            }
        }
    }


    for(ix=0; ix<nx; ix++){
        sf_floatwrite(vdep[ix], nt, Fvdep);
        if(NULL != Fvdix) sf_floatwrite(vdix[ix], nt, Fvdix);
    }
    if(verb) fprintf(stderr, "Output velocity written to stdout.\n");

    sf_maxa_free(ay);
    free(vmig[0]);
    free(vmig);
    free(vdep[0]);
    free(vdep);
    free(vdix[0]);
    free(vdix);
    free(vdix_dt[0]);
    free(vdix_dt);
    free(vdix_dg1[0]);
    free(vdix_dg1);
    free(vdix_dg2[0]);
    free(vdix_dg2);
    free(vdix_dg1g1[0]);
    free(vdix_dg1g1);
    free(vdix_dg1g2[0]);
    free(vdix_dg1g2);
    free(vdix_dg2g2[0]);
    free(vdix_dg2g2);
    sf_close();
    exit (0);
}
