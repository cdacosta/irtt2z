/* irtt2z  subroutines */
/*
  Copyright (C) 2013 Carlos Alberto da Costa Filho
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <rsf.h>
#include <math.h>
/*^*/

#include "irtt2z.h"

static float azim, h;
static sf_axa at, ax, ay, az;
static sf_axa ag1, ag2;
static float **vmig;

/*------------------------------------------------------------*/

void irtt2z_init(float **vmig_in /* Migration velocity */,
        sf_axis at_in /* t axis */,
        sf_axis ax_in /* x axis */,
        sf_axis ay_in /* y axis */,
        sf_axis ag1_in /* g1 axis */,
        sf_axis ag2_in /* g2 axis */,
        float azim_in /* Azimuth */,
        float h_in /* Ray-tracing step */)
/*< Initialize irtt2z >*/
{
    vmig = vmig_in;
    at = sf_nod(at_in);
    ax = sf_nod(ax_in);
    ay = sf_nod(ay_in);
    ag1 = sf_nod(ag1_in);
    ag2 = sf_nod(ag2_in);
    azim = azim_in;
    h = h_in;
}

float **irtt2z_dt(float** inp)
/*< Partial derivative d/dT
 *  out[0] points to a contiguous array >*/
{
    int i1, i2;
    float **out;

    out = sf_floatalloc2(at.n, ag1.n);
    for(i2=0; i2<ag1.n; i2++){
        out[i2][0] = dif1(inp[i2][1], inp[i2][0], at.d);
        for(i1=1; i1<at.n-1; i1++){
            out[i2][i1] = dif1(inp[i2][i1+1], inp[i2][i1-1], 2*at.d);
        }
        out[i2][at.n-1] = dif1(inp[i2][at.n-1], inp[i2][at.n-2], at.d);
    }
    return out;
}

float **irtt2z_dg1(float** inp)
/*< Partial derivative d/dg1
 *  out[0] points to a contiguous array >*/
{
    int i1, i2;
    float **out;

    out = sf_floatalloc2(at.n, ag1.n);
    for(i1=0; i1<at.n; i1++){
        out[0][i1] = dif1(inp[1][i1], inp[0][i1], ag1.d);
        for(i2=1; i2<ag1.n-1; i2++){
            out[i2][i1] = dif1(inp[i2+1][i1], inp[i2-1][i1], 2*ag1.d);
        }
        out[ag1.n-1][i1] = dif1(inp[ag1.n-1][i1], inp[ag1.n-2][i1], ag1.d);
    }
    return out;
}

float **irtt2z_dg2(float** inp)
/*< Partial derivative d/dg2
 *  out[0] points to a contiguous array >*/
{
    int i1, i2;
    float **out;

    out = sf_floatalloc2(at.n, ag1.n);
    for(i1=0; i1<at.n; i1++){
        for(i2=0; i2<ag1.n; i2++){
            out[i2][i1] = 0.0;
        }
    }
    return out;
}

float **irtt2z_dg1g1(float** inp)
/*< Partial derivative d^2/dg1^2
 *  out[0] points to a contiguous array >*/
{
    int i1, i2;
    float **out;

    out = sf_floatalloc2(at.n, ag1.n);
    for(i1=0; i1<at.n; i1++){
        out[0][i1] = dif2(inp[2][i1], inp[1][i1], inp[0][i1], ag1.d);
        for(i2=1; i2<ag1.n-1; i2++){
            out[i2][i1] = dif2(inp[i2+1][i1],
                               inp[i2][i1],
                               inp[i2-1][i1],
                               ag1.d);
        }
        out[ag1.n-1][i1] = dif2(inp[ag1.n-1][i1],
                                inp[ag1.n-2][i1],
                                inp[ag1.n-3][i1],
                                ag1.d);
    }
    return out;
}

float **irtt2z_dg1g2(float **inp)
/*< Partial derivative d^2/dg1dg2
 *  out[0] points to a contiguous array >*/
{
    return irtt2z_dg2(inp);
}

float **irtt2z_dg2g2(float** inp)
/*< Partial derivative d^2/dg2^2
 *  out[0] points to a contiguous array >*/
{
    return irtt2z_dg2(inp);
}

float **irtt2z_dix(float **inp)
/*< Dix conversion of vmig
 *  out[0] points to a contiguous array >*/
{
    int i1, i2;
    float **out;
    float tv2_0, tv2_1;

    out = sf_floatalloc2(at.n, ax.n);

    for(i2=0; i2<ax.n; i2++){
        /* tv2_0 holds the value of t*v^2 for the sample before. */
        /* tv2_1 holds the value of t*v^2 for the sample after. */
        /* They will be used to find d/dt with a finite difference scheme. */
        tv2_0 = at.o*inp[i2][0]*inp[i2][0];
        tv2_1 = (at.o + at.d)*inp[i2][1]*inp[i2][1];
        out[i2][0] = sqrt(dif1(tv2_1, tv2_0, at.d));
        for(i1=1; i1<at.n-1; i1++){
            tv2_0 = (at.o + (i1-1)*at.d)*inp[i2][i1-1]*inp[i2][i1-1];
            tv2_1 = (at.o + (i1+1)*at.d)*inp[i2][i1+1]*inp[i2][i1+1];
            out[i2][i1] = sqrt(dif1(tv2_1, tv2_0, 2*at.d));
        }
        tv2_0 = (at.o + (at.n-2)*at.d)*inp[i2][i1-2]*inp[i2][i1-2];
        tv2_1 = (at.o + (at.n-1)*at.d)*inp[i2][i1-1]*inp[i2][i1-1];
        out[i2][at.n-1] = sqrt(dif1(tv2_1, tv2_0, at.d));
    }
    return out;
}

float irtt2z_l_interp(float x, float t, float **inp)
/*< Returns vdix at an arbitrary position (x, t).
 *  Uses bilinear interpolation from 4 grid points to obtain the value. >*/
{
    float val;
    int ix1, ix2, it1, it2;
    float x1, x2, t1, t2;
    float ix = (x - ax.o)/ax.d;
    float it = (t - at.o)/at.d;

    ix1 = floor(ix);
    ix2 = ix1 + 1;
    it1 = floor(it);
    it2 = it1 + 1;

    x1 = ax.o + ix1*ax.d;
    x2 = ax.o + ix2*ax.d;
    t1 = at.o + it1*at.d;
    t2 = at.o + it2*at.d;

    if ((ix1 >= 0) && (ix2 < ax.n) && (it1 >= 0) && (it2 < at.n)){
        if ((ix1 == ix) && (it1 != it)){
            val  = (t2 - t)*inp[ix1][it1] + (t - t1)*inp[ix1][it2];
            val /= (t2 - t1);
        } else if ((ix1 != ix) && (it1 == it)) {
            val  = (x2 - x)*inp[ix1][it1] + (x - x1)*inp[ix2][it1];
            val /= (x2 - x1);
        } else {
            val  = (x2 - x)*(t2 - t)*inp[ix1][it1];
            val += (x - x1)*(t2 - t)*inp[ix2][it1];
            val += (x2 - x)*(t - t1)*inp[ix1][it2];
            val += (x - x1)*(t - t1)*inp[ix2][it2];
            val /= (x2 - x1)*(t2 - t1);
        }
        return val;
    } else {
        return -1;
    }
}

float irtt2z_rk4_v2p(float x, float v, float p)
/*< Calculates next step of dx/dT = v^2 p
 *  It also calculates dQi/dT = v^2 Pi >*/
{
    float k1, k2, k3, k4, inc;
    k1 = v*v*p;
    inc = 0.5*h*k1;
    k2 = v*v*(p + inc);
    inc = 0.5*h*k2;
    k3 = v*v*(p + inc);
    inc = h*k3;
    k4 = v*v*(p + inc);

    return x + h*(k1+2*k2+2*k3+k4)/6.0;
}

float irtt2z_rk4_dPdt(float P, float v, float VQ)
/*< Calculates next step of dPi/dT = -VQi/v >*/
{
    float k1, k2, k3, k4, inc;
    k1 = -VQ/v;
    inc = 0.5*h*k1;
    k2 = -(VQ + inc)/v;
    inc = 0.5*h*k2;
    k3 = -(VQ + inc)/v;
    inc = h*k3;
    k4 = -(VQ + inc)/v;

    return P + h*(k1+2*k2+2*k3+k4)/6.0;
}

float irtt2z_rk4_de1dt(float e1[3], float v, float p[3], float dpdt[3],
                       int ind)
/*< Calculates next step of de1/dT = -v^2(e1 . dp/dt) p for index ind >*/
{
    int i;
    float k1, k2, k3, k4, inc, dot=0;
    for (i=0; i<3; i++){
        dot += e1[i]*dpdt[i];
    }
    k1 = -v*v*dot*p[ind];

    inc = 0.5*h*k1;
    dot = 0;
    for (i=0; i<3; i++){
        dot += e1[i]*dpdt[i];
        if (i==ind)
            dot += inc*dpdt[i];
    }
    k2 = -v*v*dot*p[ind];

    inc = 0.5*h*k2;
    dot = 0;
    for (i=0; i<3; i++){
        dot += e1[i]*dpdt[i];
        if (i==ind)
            dot += inc*dpdt[i];
    }
    k3 = -v*v*dot*p[ind];

    inc = h*k3;
    dot = 0;
    for (i=0; i<3; i++){
        dot += e1[i]*dpdt[i];
        if (i==ind)
            dot += inc*dpdt[i];
    }
    k4 = -v*v*dot*p[ind];

    return e1[ind] + h*(k1+2*k2+2*k3+k4)/6.0;
}

float irtt2z_A(float Q1[2][2], float Q2[2][2], float u[2])
/*< Calculates A = u^T Q2^(-1) Q1 u >*/
{
    float y[2];
    /* y = Q1 u */
    mv_mult(Q1, u, y);
    /*sf_warning("irtt2z_A (y0, y1) = (%.2f, %.2f)", y[0], y[1]);*/
    /* y = Q2^-1 Q1 u */
    mv_mult_by_inv(Q2, y, y);
    /*sf_warning("irtt2z_A (y0, y1) = (%.2f, %.2f)", y[0], y[1]);*/
    return u[0]*y[0] + u[1]*y[1];
}

float irtt2z_B(float Q2[2][2], float u[2])
/*< Calculates B = u^T Q2^(-1) Q2^(-T) u >*/
{
    float Q2T[2][2] = {{Q2[0][0], Q2[1][0]}, {Q2[0][1], Q2[1][1]}};
    float y[2];
    /* y = Q2^-T u */
    mv_mult_by_inv(Q2T, u, y);
    /* y = Q2^-1 Q2^-T u */
    mv_mult_by_inv(Q2, y, y);
    return u[0]*y[0] + u[1]*y[1];
}

float irtt2z_dBdt(float Q2[2][2], float P2[2][2], float u[2])
/*< Calculates dB/dt = u^T Q2^(-1) P2 Q2^(-1) Q2^(-T) u >*/
{
    float Q2T[2][2] = {{Q2[0][0], Q2[1][0]}, {Q2[0][1], Q2[1][1]}};
    float y[2];
    /* y = Q2^-T u */
    mv_mult_by_inv(Q2T, u, y);
    /* y = Q2^-1 Q2^-T u */
    mv_mult_by_inv(Q2, y, y);
    /* y = P2 Q2^-1 Q2^-T u */
    mv_mult(P2, y, y);
    /* y = Q2^(-1) P2 Q2^-1 Q2^-T u */
    mv_mult_by_inv(Q2, y, y);
    return u[0]*y[0] + u[1]*y[1];
}

void irtt2z_cross(float u[3], float v[3], float y[3])
/*< Calculates y = u x v >*/
{
    int ii;
    float t1[3];
    float t2[3];
    for (ii=0; ii<4; ii++){
        t1[ii] = u[ii];
        t2[ii] = v[ii];
    }

    y[0] = t1[1]*t2[2] - t1[2]*t2[1];
    y[1] = t1[2]*t2[0] - t1[0]*t2[2];
    y[2] = t1[0]*t2[1] - t1[1]*t2[0];
}

void irtt2z_mm_mult(float H[3][3], float Q1q[3][3], float Q1x[3][3])
/*< Calculates Q1x = H Q1q >*/
{
    int ii, jj, kk;
    float sum;
    for(ii=0; ii<3; ii++){
        for(jj=0; jj<3; jj++){
            sum = 0;
            for(kk=0; kk<4; kk++){
                sum += H[ii][kk]*Q1q[kk][jj];
            }
            Q1x[ii][jj] = sum;
        }
    }
}

/*------------------------------------------------------------*/
float dif1(float forwd, float backwd, float step)
/*< Difference over step >*/
{
       return (forwd - backwd)/step;
}

float dif2(float forwd, float central, float backwd, float step)
/*< Second difference over step^2 >*/
{
       return (forwd - 2*central + backwd)/(step*step);
}

void mv_mult(float M[2][2], float x[2], float y[2])
/*< y = Mx, M is 2x2 >*/
{
    float tmp[2];
    tmp[0] = x[0];
    tmp[1] = x[1];
    y[0] = M[0][0]*tmp[0] + M[0][1]*tmp[1];
    y[1] = M[1][0]*tmp[0] + M[1][1]*tmp[1];;
}

void mv_mult_by_inv(float M[2][2], float x[2], float y[2])
/*< y = M^(-1) x, M is 2x2 >*/
{
    float tmp[2];
    tmp[0] = x[0];
    tmp[1] = x[1];
    y[0] = (M[1][1]*tmp[0] - M[0][1]*tmp[1])/(M[0][0]*M[1][1]-M[1][0]*M[0][1]);
    y[1] = (M[0][0]*tmp[1] - M[1][0]*tmp[0])/(M[0][0]*M[1][1]-M[1][0]*M[0][1]);
}
